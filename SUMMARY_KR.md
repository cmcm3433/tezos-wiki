# Summary

* [Introduction to the Tezos Wiki](README.md)
* [Tezos 벡서](files/whitepaper.md)

### Tezos Basics

* [Tezos 는 무엇인가요??](files/basics.md#intro)
* [무엇이 Tezos를 유니크 하게 만드나요?](files/basics.md#unique)
* [Which use cases are uniquely suited to Tezos?](files/basics.md#use-case)

### 지분증명 (PoS,Proof-of-Stake)
* [지분증명(Proof-of-Stake)은 무엇인가요?](files/proofofstake.md#intro)
* [Tezos의 합의(consensus) 알고리즘](files/proofofstake.md#consensus)

### Baking
* [베이킹(baking)은 무엇인가요?](files/baking.md#what)
* [위임(delegating)은 무엇인가요?](files/baking.md#delegate)
* [베이킹을 해야하나요 아니면 위임(delegate)을 해야하나요?](files/baking.md#bakeordelegate)
* [베이커(baker) 고르기](files/baking.md#bakerselection)

### Tezos 거버넌스
* [자가개정(Self Amendment)은 무엇인가요?](files/self-amendment.md#introduction)
* [자가 개정은 어떻게 동작하나요?](files/self-amendment.md#how)
* [오프체인 거버넌스(Off-chain Governance)](files/self-amendment.md#offchain)

### Tezos의 스마트 컨트랙트
* [스마트 컨트랙트 프로그래밍 언어 소개](files/language.md#faq)
* [스마트 컨트랙트 언어 - 미켈슨(Michelson)](files/language.md#michelson)
* [스마트 컨트랙트 언어 - 리퀴디티(Liquidity)](files/language.md#liquidity)
* [프로그래밍 언어 참고자료](files/language.md#resources)

### 형식 검증(Formal Verification)
* [형식 검증에 대한 소개](files/formal-verification.md#intro)
* [미켈슨(Michelson) 그리고 GADT](files/formal-verification.md#gadt)
* [미켈슨(Michelson 그리고 Coq](files/formal-verification.md#coq)

### Future Developments
* [프라이버시(Privacy)](files/future.md#intro)
* [합의 알고리즘(consensus)](files/future.md#consensus)
* [레이어2 ](files/future.md#layer2)
* [거버넌스](files/future.md#governance)

### Built on Tezos
* [Tezos Projects](https://tezosprojects.com/index.html)

### 참고자료
* [Tezos 재단](files/resources.md#foundation)
* [Tezos Protocol 개발 지원 Programs](files/resources.md#grant)
* [Tezos 지갑](files/resources.md#wallet)
* [블록 탐색기(Block Explorer)](files/resources.md#explorer)