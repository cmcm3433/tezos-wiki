# Summary

* [Introduction to the Tezos Wiki](README.md)
* [Tezos Whitepaper](files/whitepaper.md)

### Tezos Basics

* [What is Tezos?](files/basics.md#intro)
* [What makes Tezos unique?](files/basics.md#unique)
* [Which use cases are uniquely suited to Tezos?](files/basics.md#use-case)

### Proof-of-Stake
* [What is Proof-of-Stake?](files/proofofstake.md#intro)
* [The Tezos consensus algorithm](files/proofofstake.md#consensus)

### Baking
* [What is baking?](files/baking.md#what)
* [What is delegating?](files/baking.md#delegate)
* [Should I bake or delegate?](files/baking.md#bakeordelegate)
* [Selecting a baker](files/baking.md#bakerselection)

### Tezos Governance
* [What is Self Amendment?](files/self-amendment.md#introduction)
* [How does it work?](files/self-amendment.md#how)
* [Off-chain Governance](files/self-amendment.md#offchain)

### Smart Contracts in Tezos
* [Introduction](files/language.md#faq)
* [Michelson Language](files/language.md#michelson)
* [Liquidity Language](files/language.md#liquidity)
* [Language Resources](files/language.md#resources)

### Formal Verification
* [Introduction](files/formal-verification.md#intro)
* [Michelson and GADT](files/formal-verification.md#gadt)
* [Michelson and Coq](files/formal-verification.md#coq)

### Future Developments
* [Privacy](files/future.md#intro)
* [Consensus](files/future.md#consensus)
* [Layer 2](files/future.md#layer2)
* [Governance](files/future.md#governance)

### Built on Tezos
* [Tezos Projects](https://tezosprojects.com/index.html)

### Resources
* [Tezos Foundation](files/resources.md#foundation)
* [Tezos Grant Programs](files/resources.md#grant)
* [Wallets](files/resources.md#wallet)
* [Block Explorer](files/resources.md#explorer)