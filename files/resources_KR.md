---
layout: post
title:  "Resources"
date:   2019-04-18 22:42:18
---
Resources
===========

# Tezos Group 리스트 {#foundation_KR}

- [Tezos 재단](https://tezos.foundation/)
- [Tezos 커먼즈 재단](https://tezoscommons.org/)
- [Tezos 재팬](https://twitter.com/TezosJapan)
- [Tezos 코리아](http://tezoskoreacommunity.org/)
- [Tezos 리우](https://tezos.rio/)
- [Tezos 코리아 재단](http://tezoskorea.foundation/)
- [Tezos 동남 아시아](https://www.tezos.org.sg/)
- [Tezos 몬트리올](https://tezosmtl.com/)
- [Tezos 오카멜 미켈슨 인스티튜트](https://tomi.institute/)
- [Tezos 디트로이트](https://twitter.com/TezosD)
- [Tezos 룩셈베르크](https://tezos.lu/)
- [Tezos 터키](https://twitter.com/tezosturkey)
- [Tezos 제네바](https://twitter.com/TezosGeneva)

# Tezos 재단 지원금 신청 {#grant_KR}

[여기에서](https://tezos.foundation/grants-overview) Tezos 에코시스템을 위한 프로젝트 펀딩을 신청하세요.

# 테조스 지갑들 {#wallet_KR}
- [Tezbox (모바일/데스크탑)](https://tezbox.com/)
- [Galleon (Desktop)](https://galleon-wallet.tech/)
- [Cortez (모바일-안드로이드만)](https://play.google.com/store/apps/details?id=com.tezcore.cortez)
- [Kukai (Desktop)](https://kukai.app/)

# 블록 익스플로러{#explorer_KR}
- [Tzscan](https://tzscan.io/)
- [Tezex](https://tezex.info/)

# 베이킹 서비스들{#baking_KR}

[여기에서](https://mytezosbaker.com/) 베이커들의 리스트를 찾을 수 있습니다.